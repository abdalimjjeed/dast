from unittest import TestCase
from unittest.mock import patch

from src.config import InvalidConfigurationError
from src.dast_error_handler import DASTErrorHandler


class TestDASTErrorHandler(TestCase):

    @patch('src.dast_error_handler.logging')
    def test_should_log_unhandled_exception(self, mock_logging):
        error = RuntimeError('simulated.error')

        DASTErrorHandler().handle(error)

        self.assertEqual(1, len(mock_logging.error.mock_calls))
        self.assertIn('Unhandled exception', mock_logging.error.mock_calls[0][1][0])
        self.assertEqual(error, mock_logging.error.mock_calls[0][2]['exc_info'])

    @patch('src.dast_error_handler.logging')
    def test_should_not_print_stack_trace_for_known_errors(self, mock_logging):
        error = InvalidConfigurationError('missing -t target')

        DASTErrorHandler().handle(error)

        self.assertEqual(1, len(mock_logging.error.mock_calls))
        self.assertIn('InvalidConfigurationError: missing -t target', mock_logging.error.mock_calls[0][1][0])
        self.assertEqual({}, mock_logging.error.mock_calls[0][2])
