from src.models.rules import Rules
from .f_rule import f_rule


def f_rules(*rls) -> Rules:
    # f_rules() returns rules with one rule
    if not len(rls):
        return Rules([f_rule()])

    # f_rules(None) returns rules with no rule
    if len(rls) == 1 and not rls[0]:
        return Rules([])

    # f_rules(rule(), rule()) returns given rules
    return Rules(list(rls))
