from unittest import TestCase
from unittest.mock import DEFAULT, MagicMock, patch

from src.services.browserker import SecureReportParser


class TestSecureReportParser(TestCase):

    def setUp(self) -> None:
        self.json_response = {
            'scan': {
                'scanned_resources': [
                    {
                        'method': 'GET',
                        'type': 'url',
                        'url': 'http://pancakes/',
                    },
                    {
                        'method': 'POST',
                        'type': 'url',
                        'url': 'http://pancakes/1.html',
                    },
                ],
            },
            'vulnerabilities': [
                {
                    'category': 'dast',
                    'confidence': 'High',
                    'cve': '693.1',
                },
                {
                    'category': 'dast',
                    'confidence': 'Medium',
                    'cve': '82',
                },
            ],
        }

    def test_parsers_scanned_resources(self):
        with patch.multiple(
                'src.services.browserker.secure_report_parser',
                open=DEFAULT,
                os=DEFAULT,
                json=DEFAULT) as mock_dependencies:
            mock_path = mock_dependencies['os']
            mock_path.path.isfile = MagicMock(return_value=True)
            mock_json = mock_dependencies['json']
            mock_json.load = MagicMock(return_value=self.json_response)
            parser = SecureReportParser('/path/to/file')
            result = parser.parse_scanned_resources()

        self.assertEqual(2, len(result))

    def test_scanned_resources_are_empty_when_report_does_not_exist(self):
        with patch.multiple(
                'src.services.browserker.secure_report_parser',
                open=DEFAULT,
                os=DEFAULT,
                json=DEFAULT) as mock_dependencies:
            mock_path = mock_dependencies['os']
            mock_path.path.isfile = MagicMock(return_value=False)
            mock_json = mock_dependencies['json']
            mock_json.load = MagicMock(return_value=self.json_response)
            parser = SecureReportParser('/path/to/file')
            result = parser.parse_scanned_resources()

        self.assertEqual([], result)

    def test_scanned_resources_are_empty_when_there_is_no_scan(self):
        with patch.multiple(
                'src.services.browserker.secure_report_parser',
                open=DEFAULT,
                os=DEFAULT,
                json=DEFAULT) as mock_dependencies:
            mock_path = mock_dependencies['os']
            mock_path.path.isfile = MagicMock(return_value=True)
            mock_json = mock_dependencies['json']
            mock_json.load = MagicMock(return_value={})
            parser = SecureReportParser('/path/to/file')
            result = parser.parse_scanned_resources()

        self.assertEqual([], result)

    def test_scanned_resources_are_empty_when_scan_is_none(self):
        with patch.multiple(
                'src.services.browserker.secure_report_parser',
                open=DEFAULT,
                os=DEFAULT,
                json=DEFAULT) as mock_dependencies:
            mock_path = mock_dependencies['os']
            mock_path.path.isfile = MagicMock(return_value=True)
            mock_json = mock_dependencies['json']
            mock_json.load = MagicMock(return_value={'scan': None})
            parser = SecureReportParser('/path/to/file')
            result = parser.parse_scanned_resources()

        self.assertEqual([], result)

    def test_scanned_resources_are_empty_when_there_are_no_scanned_resources(self):
        with patch.multiple(
                'src.services.browserker.secure_report_parser',
                open=DEFAULT,
                os=DEFAULT,
                json=DEFAULT) as mock_dependencies:
            mock_path = mock_dependencies['os']
            mock_path.path.isfile = MagicMock(return_value=True)
            mock_json = mock_dependencies['json']
            mock_json.load = MagicMock(return_value={'scan': {'scanned_resources': []}})
            parser = SecureReportParser('/path/to/file')
            result = parser.parse_scanned_resources()

        self.assertEqual([], result)

    def test_scanned_resources_are_empty_when_scanned_resources_are_none(self):
        with patch.multiple(
                'src.services.browserker.secure_report_parser',
                open=DEFAULT,
                os=DEFAULT,
                json=DEFAULT) as mock_dependencies:
            mock_path = mock_dependencies['os']
            mock_path.path.isfile = MagicMock(return_value=True)
            mock_json = mock_dependencies['json']
            mock_json.load = MagicMock(return_value={'scan': {'scanned_resources': None}})
            parser = SecureReportParser('/path/to/file')
            result = parser.parse_scanned_resources()

        self.assertEqual([], result)

    def test_parsers_vulnerabilities(self):
        with patch.multiple(
                'src.services.browserker.secure_report_parser',
                open=DEFAULT,
                os=DEFAULT,
                json=DEFAULT) as mock_dependencies:
            mock_path = mock_dependencies['os']
            mock_path.path.isfile = MagicMock(return_value=True)
            mock_json = mock_dependencies['json']
            mock_json.load = MagicMock(return_value=self.json_response)
            parser = SecureReportParser('/path/to/file')
            result = parser.parse_vulnerabilities()

        self.assertEqual(2, len(result))

    def test_vulnerabilities_are_empty_when_report_does_not_exist(self):
        with patch.multiple(
                'src.services.browserker.secure_report_parser',
                open=DEFAULT,
                os=DEFAULT,
                json=DEFAULT) as mock_dependencies:
            mock_path = mock_dependencies['os']
            mock_path.path.isfile = MagicMock(return_value=False)
            mock_json = mock_dependencies['json']
            mock_json.load = MagicMock(return_value=self.json_response)
            parser = SecureReportParser('/path/to/file')
            result = parser.parse_vulnerabilities()

        self.assertEqual([], result)

    def test_vulnerabilities_are_empty_when_has_no_vulnerabilities(self):
        with patch.multiple(
                'src.services.browserker.secure_report_parser',
                open=DEFAULT,
                os=DEFAULT,
                json=DEFAULT) as mock_dependencies:
            mock_path = mock_dependencies['os']
            mock_path.path.isfile = MagicMock(return_value=True)
            mock_json = mock_dependencies['json']
            mock_json.load = MagicMock(return_value={})
            parser = SecureReportParser('/path/to/file')
            result = parser.parse_vulnerabilities()

        self.assertEqual([], result)

    def test_vulnerabilities_are_empty_when_vulnerabilities_are_none(self):
        with patch.multiple(
                'src.services.browserker.secure_report_parser',
                open=DEFAULT,
                os=DEFAULT,
                json=DEFAULT) as mock_dependencies:
            mock_path = mock_dependencies['os']
            mock_path.path.isfile = MagicMock(return_value=True)
            mock_json = mock_dependencies['json']
            mock_json.load = MagicMock(return_value={'vulnerabilities': None})
            parser = SecureReportParser('/path/to/file')
            result = parser.parse_vulnerabilities()

        self.assertEqual([], result)
