from unittest import TestCase
from unittest.mock import DEFAULT, Mock, patch

from src.models import Target
from src.services import TargetSelector


class TestTargetSelector(TestCase):

    def setUp(self):
        self.config = Mock()
        self.zap = Mock()

    def test_uses_given_target_for_website_scan(self):
        self.config.is_api_scan = False
        self.config.target = 'https://dast_target.com'
        selector = TargetSelector(self.zap, self.config)

        with patch('src.services.target_selector.logging') as mock_logging:
            target = selector.select()

        mock_logging.info.assert_called_once_with('Using scan target https://dast_target.com')

        self.assertEqual(target, 'https://dast_target.com')

    def test_select_resets_target_to_host_if_spider_start_at_host(self):
        self.zap.ping_target.return_value = 'Success'
        self.config.is_api_scan = False
        self.config.spider_start_at_host = True
        self.config.target = 'https://dast_target.com/some_path'
        selector = TargetSelector(self.zap, self.config)

        with patch('src.services.target_selector.logging') as mock_logging:
            target = selector.select()

        mock_logging.debug.assert_called_once_with(
            'Setting target to normalized URL: https://dast_target.com',
        )
        mock_logging.info.assert_called_once_with('Using scan target https://dast_target.com')

        self.assertEqual(target, 'https://dast_target.com')

    def test_select_does_not_reset_target_to_host_if_not_spider_start_at_host(self):
        self.zap.ping_target.return_value = 'Success'
        self.config.is_api_scan = False
        self.config.spider_start_at_host = False
        self.config.target = 'https://dast_target.com/some_path'
        selector = TargetSelector(self.zap, self.config)

        with patch('src.services.target_selector.logging') as mock_logging:
            target = selector.select()

        mock_logging.info.assert_called_once_with('Using scan target https://dast_target.com/some_path')

        self.assertEqual(target, 'https://dast_target.com/some_path')

    def test_select_uses_api_target_for_api_scan(self):
        api_urls = Mock()
        self.config.api_specification = 'https://api.spec'
        self.config.zap_api_host_override = 'https://host.override'
        self.config.is_api_scan = True
        selector = TargetSelector(self.zap, self.config)
        target = None

        with patch.multiple(
            'src.services.target_selector',
            APISpecification=DEFAULT,
            APITargetSelector=DEFAULT,
            logging=DEFAULT,
        ) as mock_dependencies:
            mock_specification = mock_dependencies['APISpecification']
            mock_api_target = mock_dependencies['APITargetSelector']
            mock_specification.return_value.load.return_value = api_urls
            mock_api_target.return_value.select.return_value = Target('https://host.override/api/v1')

            target = selector.select()

        mock_specification.assert_called_once_with(self.zap, 'https://api.spec', 'https://host.override')
        mock_specification.return_value.load.assert_called_once()
        mock_api_target.assert_called_once_with(api_urls, 'https://host.override')

        self.assertEqual(target, 'https://host.override')
