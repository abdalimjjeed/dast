#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

BUILT_IMAGE=${BUILT_IMAGE:-dast}
MAX_SCAN_DURATION_SECONDS=${MAX_SCAN_DURATION_SECONDS:-66}

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

setup_suite() {
  setup_test_dependencies
  run_basic_site
  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker rm --force opt-param-test >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_merge_browserker_report_14x() {
  skip_if_fips "Expectations are ZAP-specific"

  docker run --rm \
    -v "${PWD}":/output \
    -v "${PWD}/fixtures/scripts":/home/zap/custom-scripts \
    --network test \
    --env DAST_BROWSER_SCAN=true \
    --env DAST_FULL_SCAN_ENABLED=true \
    --env CI_SERVER_VERSION_MAJOR=15 \
    --env CI_SERVER_VERSION_MINOR=3 \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx >output/test_merge_browserker_report_14x.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_merge_browserker_report_14x.json

  assert_output test_merge_browserker_report_14x output/report_test_merge_browserker_report_14x

  ./verify-dast-schema.py output/report_test_merge_browserker_report_14x.json
}

test_merge_browserker_report_15x() {
  skip_if_fips "Expectations are ZAP-specific"

  docker run --rm \
    -v "${PWD}":/output \
    -v "${PWD}/fixtures/scripts":/home/zap/custom-scripts \
    --network test \
    --env DAST_BROWSER_SCAN=true \
    --env DAST_FULL_SCAN_ENABLED=true \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx >output/test_merge_browserker_report_15x.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_merge_browserker_report_15x.json

  assert_output test_merge_browserker_report_15x output/report_test_merge_browserker_report_15x

  ./verify-dast-schema.py output/report_test_merge_browserker_report_15x.json
}
