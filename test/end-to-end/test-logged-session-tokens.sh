#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  setup_test_dependencies

  docker network create test >/dev/null

    docker run \
    --name logged-session-tokens \
    -v "${PWD}/fixtures/auth-site":/usr/share/nginx/html:ro \
    -v "${PWD}/fixtures/auth-site/nginx.conf":/etc/nginx/conf.d/default.conf \
    --network test -d nginx:1.22.0 >/dev/null

  true
}

teardown_suite() {
  docker rm -f logged-session-tokens >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_logged_session_tokens() {
  skip_if_fips "Test enables AJAX spider"

  docker run --rm \
    -v "${PWD}":/output \
    --network test \
    --env DAST_DEBUG=1 \
    "${BUILT_IMAGE}" /analyze -j -d -t http://logged-session-tokens \
    --auth-url http://logged-session-tokens/login-form? \
    --auth-verification-url http://logged-session-tokens/home \
    --auth-username "user" \
    --auth-password "password" \
    --auth-username-field "css:[name=username]" \
    --auth-password-field "css:[name=password]" \
    --auth-submit-field "css:button[type=submit]" \
    --auth-exclude-urls "http://logged-session-tokens/users" \
    >output/test_logged_session_tokens.log 2>&1

  assert_equals "0" "$?" "Expected to exit without errors"

  grep -q "SgLz6rfax23XdQVM4mvvy%3Fa%2B3Qw" output/test_logged_session_tokens.log
  assert_equals "1" "$?" "Session token should not be logged"

  grep -q "SgLz6rfax23XdQVM4mvvy?a+3Qw" output/test_logged_session_tokens.log
  assert_equals "1" "$?" "Session token should not be logged"
}
