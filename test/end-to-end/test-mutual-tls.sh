#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

BUILT_IMAGE=${BUILT_IMAGE:-dast}

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

setup_suite() {
  setup_test_dependencies
  run_mutual_tls_site
  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_mutual_tls() {
  skip_if_fips "Expectations are ZAP-specific"

  docker run --rm \
    -v "${PWD}":/output \
    --env DAST_PKCS12_CERTIFICATE_BASE64="$(cat "${PWD}/fixtures/mutual-tls/certs/client.pfx.base64")" \
    --env DAST_PKCS12_PASSWORD="dast" \
    --network test \
    "${BUILT_IMAGE}" /analyze -d -t https://nginx >output/test_mutual_tls.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_mutual_tls.json

  assert_output test_mutual_tls output/report_test_mutual_tls

  ./verify-dast-schema.py output/report_test_mutual_tls.json
  assert_equals "0" "$?" "DAST report does not conform to DAST schema"
}

test_mutual_tls_browser_based() {
  skip_if_fips "Auth is not working: receiving 403 instead of 200"

  CERT_BASE64=$(cat "${PWD}/fixtures/mutual-tls/certs/client.pfx.base64")

  docker run --rm \
    -v "${PWD}":/output \
    --env DAST_BROWSER_SCAN=true \
    --env DAST_FULL_SCAN_ENABLED=true \
    --env DAST_FF_ENABLE_BROWSER_BASED_ATTACKS="true" \
    --env DAST_BROWSER_LOG="brows:debug,chrom:trace" \
    --env DAST_BROWSER_DEVTOOLS_LOG="Default:suppress; Fetch:messageAndBody,truncate:2000; Network:messageAndBody,truncate:2000" \
    --env DAST_PKCS12_CERTIFICATE_BASE64="$CERT_BASE64" \
    --env DAST_PKCS12_PASSWORD="dast" \
    --network test \
    "${BUILT_IMAGE}" /analyze -d -t https://nginx >output/test_mutual_tls_browser_based.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_mutual_tls_browser_based.json

  assert_output test_mutual_tls_browser_based output/report_test_mutual_tls_browser_based

  ./verify-dast-schema.py output/report_test_mutual_tls_browser_based.json
  assert_equals "0" "$?" "DAST report does not conform to DAST schema"

  grep -q "$CERT_BASE64" output/test_mutual_tls_browser_based.log
  assert_equals "1" "$?" "Certificate was found in the logs"
}
