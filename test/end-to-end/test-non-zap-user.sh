#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  setup_test_dependencies
  run_basic_site
  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_non_zap_user() {
  # The DAST report isn't tested here because of permission errors whilst running on GitLab CI
  # 666 is a user id that is not present on the DAST image
  docker run --rm \
    --network test \
    --user=666 \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx >output/test_non_zap_user.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  if [[ "$FIPS_MODE" == "1" ]]; then
    grep -q "Browserker completed with exit code 0" output/test_non_zap_user.log
    assert_equals "0" "$?" "Scan did not complete"
  else
    grep -q "SUMMARY - PASS" output/test_non_zap_user.log
    assert_equals "0" "$?" "Scan did not complete"
  fi

  grep -q 'Exception' output/test_non_zap_user.log
  assert_equals "1" "$?" "Log contains errors"
}
