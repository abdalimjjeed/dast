from os import path
from unittest import TestCase

from src.zap_gateway.zap_database import ZAPDatabase


class TestZAPDatabase(TestCase):

    def setUp(self) -> None:
        self.hsqldb_jar = path.join(path.dirname(__file__), 'lib', 'hsqldb-2.7.1.jar')
        pass

    def test_can_read_zap_database(self):
        database_dir = path.join(path.dirname(__file__), 'fixture', 'dast_database')
        database = ZAPDatabase(database_dir, self.hsqldb_jar)

        results = database.select_many('select HISTORYID, METHOD, URI '
                                       'from HISTORY '
                                       'where HISTORYID in (1, 8, 12)')

        self.assertEqual(2, len(results))
        self.assertEqual(('1', 'GET', 'http://basic_site_server/'), results[0])
        self.assertEqual(('8', 'GET', 'http://basic_site_server/sitemap.xml'), results[1])

    def test_should_throw_error_if_database_not_present(self):
        database = ZAPDatabase('not.a.directory', self.hsqldb_jar)

        with self.assertRaises(RuntimeError) as error:
            database.select_many('select * from INFORMATION_SCHEMA.TABLES')

        self.assertIn('Failed to find ZAP database', str(error.exception))
        self.assertIn('not.a.directory', str(error.exception))

    def test_database_can_be_closed_without_connecting(self):
        database = ZAPDatabase('a_directory', self.hsqldb_jar)
        database.close()
