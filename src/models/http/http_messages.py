from typing import Dict, List, Optional

from .http_message import HttpMessage, HttpMessageID


class HttpMessages:

    def __init__(self, messages: List[HttpMessage] = []):
        self._messages = messages

    def __len__(self) -> int:
        return len(self._messages)

    def __getitem__(self, message_index: int) -> HttpMessage:
        return self._messages[message_index]

    def message_with_id(self, message_id: str) -> Optional[HttpMessage]:
        for message in self._messages:
            if HttpMessageID(message_id) == message.message_id:
                return message

        return None

    def request_summaries(self) -> List[Dict[str, str]]:
        summaries = {}

        for message in self._messages:
            if message.request.url and message.request.method:
                request_hash = f'{message.request.url} {message.request.method}'
                summaries[request_hash] = {'url': message.request.url, 'method': message.request.method}

        return list(summaries.values())

    def human_readable_requests(self) -> str:
        lines = [f"{request['method']} {request['url']}" for request in self.request_summaries()]
        return '\n'.join(sorted(set(lines)))
