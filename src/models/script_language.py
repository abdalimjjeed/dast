from __future__ import annotations

from enum import Enum
from typing import Optional


# Scripts can be written in languages that support JSR 223 http://www.jcp.org/en/jsr/detail?id=223
class ScriptLanguage(Enum):
    # https://docs.oracle.com/javase/8/docs/technotes/guides/scripting/nashorn/
    JAVASCRIPT = (0, 'Oracle Nashorn', '.js')

    def zap_name(self) -> str:
        return self.value[1]

    def file_extension(self) -> str:
        return self.value[2]

    @staticmethod
    def detect_language(file_path: str) -> Optional[ScriptLanguage]:
        for language in ScriptLanguage:
            if file_path and file_path.endswith(language.file_extension()):
                return language

        return None
