import os
import sys
import uuid

from src.config import ConfigurationParser
from src.container import Container
from src.custom_hooks import CustomHooks
from src.report import SecurityReportFormatter
from src.report.security_report_formatters import ScanReportFormatter, VulnerabilityReportFormatter
from src.scan_script_wrapper import ScanScriptWrapper
from src.services.script_finder import ScriptFinder
from src.system import System
from src.zap_gateway import AlertsParser, NullZAPServer, NullZAProxy, RequestHeadersConfigurationBuilder, \
                            UserConfigurationBuilder, ZAPServer, ZAProxy


def initialize_container() -> Container:
    if Container.CACHE:
        return Container.CACHE

    c = Container()
    c.system = System()
    c.start_date_time = c.system.current_date_time()
    c.config = ConfigurationParser().parse(sys.argv[1:], os.environ)

    c.request_headers_builder = RequestHeadersConfigurationBuilder(c.config)
    c.user_configuration_builder = UserConfigurationBuilder(c.config)
    c.alerts_parser = AlertsParser()

    if c.config.zap_enabled:
        c.zap_server = ZAPServer(c.config, c.system, c.request_headers_builder, c.user_configuration_builder)
        c.zaproxy = ZAProxy(c.config.exclude_urls, c.alerts_parser, c.config)
    else:
        c.zap_server = NullZAPServer()
        c.zaproxy = NullZAProxy()

    c.script_finder = ScriptFinder(c.config.script_dirs)

    limits = c.config.security_report_character_limits
    c.vulnerability_report_formatter = VulnerabilityReportFormatter(c.config, uuid, limits)
    c.scan_report_formatter = ScanReportFormatter(c.start_date_time, c.config)
    c.security_report_formatter = SecurityReportFormatter(c.scan_report_formatter, c.vulnerability_report_formatter,
                                                          c.config)

    c.dast_report_formatter = c.security_report_formatter
    c.custom_hooks = CustomHooks(c.zaproxy, c.dast_report_formatter, c.system, c.config, c.script_finder)
    c.scan = ScanScriptWrapper(c.config, c.zap_server, c.zaproxy, c.custom_hooks)

    Container.CACHE = c
    return Container.CACHE
