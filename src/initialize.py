import logging

from .configuration import Configuration


def initialize_dast_logs(config: Configuration) -> None:
    if config.zap_debug:
        logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(message)s')
    else:
        logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
