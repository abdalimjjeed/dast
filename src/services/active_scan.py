import logging
import time
from pathlib import Path
from typing import Optional

from src.models import ContextID, Target
from src.zap_gateway import Settings, ZAProxy
from .active_scan_logger import ActiveScanLogger


class ActiveScan:

    API_SCAN_POLICY = 'API-Minimal.policy'
    DEFAULT_SCAN_POLICY = 'Default Policy.policy'
    STATUS_POLLING_INTERVAL_SECONDS = 5

    def __init__(self, zap: ZAProxy, target: Target, is_api_scan: bool):
        self._zap = zap
        self._target = target
        self._is_api_scan = is_api_scan

    def run(self) -> None:
        logging.info(f'Active Scan {self._target} with policy {self.policy_name}')

        scan_id = self._zap.run_active_scan(self._target, self.policy_name, self._context_id)
        self._wait_for_scan(scan_id)

        logging.info('Active Scan complete')

        ActiveScanLogger(self._zap.active_scan_progress(scan_id)).log()

    @property
    def policy_name(self) -> str:
        return Path(self._policy_file_name).stem

    @property
    def _policy_file_name(self) -> str:
        return self.API_SCAN_POLICY if self._is_api_scan else self.DEFAULT_SCAN_POLICY

    @property
    def _context_id(self) -> Optional[ContextID]:
        if self._is_api_scan:
            return None

        context = self._zap.context_details(Settings.CONTEXT_NAME)

        if context:
            context_id = context['id']

            logging.info(f'Active scan context: {context_id}')

            return context_id

        return None

    def _wait_for_scan(self, scan_id: int) -> None:
        time.sleep(self.STATUS_POLLING_INTERVAL_SECONDS)

        progress = self._zap.active_scan_percent_complete(scan_id)

        while progress < 100:
            logging.info(f'Active Scan progress: {progress}%')

            time.sleep(self.STATUS_POLLING_INTERVAL_SECONDS)

            progress = self._zap.active_scan_percent_complete(scan_id)
