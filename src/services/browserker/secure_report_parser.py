import json
import os
from functools import cached_property
from typing import Any, Dict, List


class SecureReportParser:

    def __init__(self, file_path: str):
        self.file_path = file_path

    def parse_vulnerabilities(self) -> List[Dict[str, Any]]:
        if not self._data.get('vulnerabilities', []):
            return []

        return self._data.get('vulnerabilities', [])

    def parse_scanned_resources(self) -> List[Dict[str, Any]]:
        if not self._data.get('scan', {}) or not self._data.get('scan', {}).get('scanned_resources', []):
            return []

        return self._data.get('scan', {}).get('scanned_resources', [])

    @cached_property
    def _data(self) -> Dict[str, Any]:
        if not os.path.isfile(self.file_path):
            return {}

        with open(self.file_path) as json_file:
            data = json.load(json_file)
            return data
