import logging
import time

from src.models import Target
from src.zap_gateway import ZAProxy


class Spider:

    SPIDER_POLL_DELAY_SECONDS = 5

    def __init__(self, zap: ZAProxy, target: Target):
        self._zap = zap
        self._target = target

    def run(self) -> None:
        logging.info(f'Spider starting with target: {self._target}')

        spider_id = self._zap.run_spider(self._target)

        time.sleep(self.SPIDER_POLL_DELAY_SECONDS)

        spider_progress_percentage = self._zap.spider_progress_percentage(spider_id)

        while spider_progress_percentage < 100:
            logging.info(f'Spider progress: {spider_progress_percentage}% complete')

            time.sleep(self.SPIDER_POLL_DELAY_SECONDS)

            spider_progress_percentage = self._zap.spider_progress_percentage(spider_id)

        logging.info('Spider complete')
