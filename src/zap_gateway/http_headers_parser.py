from typing import Optional

from src.http_header import HttpHeader
from src.http_headers import HttpHeaders


class HttpHeadersParser:

    def parse(self, raw_headers: Optional[str]) -> HttpHeaders:
        if not raw_headers:
            return HttpHeaders()

        header_lines = raw_headers.strip().splitlines()

        if len(header_lines) < 1:
            return HttpHeaders()

        headers_text = header_lines[1:]
        headers = []

        for header_text in headers_text:
            name, *values = header_text.split(': ')
            headers.append(HttpHeader(name.strip(), ': '.join(values).strip()))

        return HttpHeaders(headers)
