from typing import Optional, Tuple


class HttpStatusParser:

    # If we get a strange response from ZAP, return a status code of 0.
    # This is consistent with current ZAP behaviour.
    def parse(self, response_headers_text: Optional[str]) -> Tuple[int, str]:
        status = 0
        reason_phrase = ''

        if not response_headers_text:
            return status, reason_phrase

        lines = response_headers_text.splitlines()

        if lines:
            components = lines[0].split(' ')

            if len(components) >= 1 and components[1].isdigit():
                status = int(components[1])

            if len(components) >= 2:
                reason_phrase = ' '.join(components[2:])

        return status, reason_phrase
