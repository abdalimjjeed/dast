# Debugging private websites using the Browser Based Crawl Report

Customers often scan private websites using DAST and the Browser Based Scanner. In the case that DAST does not scan that private website as expected it can be difficult for engineers to debug the issue using logs alone. The Browser Based Crawl Report adds another level of information to assist engineers when debugging.

## Warnings

- __The crawl report is an experimental feature. It could be changed or removed at anytime.__
- __The crawl report could contain secrets.__
- __Only run the crawl report when debugging. It will increase scanning time and generate a large report.__

## Enable the crawl report

An example configuration where the crawl report debug is exported could be:

```yaml
dast:
  variables:
    DAST_WEBSITE: "https://example.com"
    ...
    DAST_BROWSER_CRAWL_REPORT: "true"
    DAST_BROWSER_MAX_ACTIONS: 100
    DAST_BROWSER_NUMBER_OF_BROWSERS: 1
  artifacts:
    paths:
      - gl-dast-debug-crawl-report.html
```

The crawl report is only enabled when `DAST_BROWSER_MAX_ACTIONS` is less than or equal to `100` and `DAST_BROWSER_NUMBER_OF_BROWSERS` is `1`.

## Reading the report

The scanner works by generating a number of paths through your site.

- path 1 load URL `http://localhost`
- path 2 load URL `http://localhost` => `click link [category shoes]`
- path 3 load URL `http://localhost` => `click link [category shoes]` => `click link [product nikes]`

It then executes each of these paths one step at a time. So to complete path 3 it would

- load URL `http://localhost`
- click link [category shoes]
- click link [product nikes]

The report is structured in the same way. The path is shown as the header and then each part of the path is listed below that header with the corresponding screenshot and DOM representation.

![doc/img/crawl_report_example.png](/doc/img/crawl_report_example.png)
