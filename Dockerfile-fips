ARG BASE_IMAGE_NAME=registry.gitlab.com/security-products/dast/browserker
ARG BASE_IMAGE_VERSION=0.0.151-fips
ARG BASE_IMAGE=$BASE_IMAGE_NAME:$BASE_IMAGE_VERSION

FROM $BASE_IMAGE as compiled_dependencies
USER root
RUN microdnf install -y --nodocs --nobest --setopt=tsflags=nodocs \
        gcc \
        python39-devel \
    ;

RUN pip3 install memory_profiler matplotlib

RUN pip3 install JPype1==1.4.0

FROM $BASE_IMAGE
ARG BUILDING_FOR=now
ARG CHROMEDRIVER_VERSION=112.0.5615.49

USER root

# Install Python and Java
# Install jq to allow users to post process the generated DAST report
RUN microdnf install -y --nodocs --nobest --setopt=tsflags=nodocs --setopt=install_weak_deps=0 \
        jq \
        wget \
        curl \
        ca-certificates \
        unzip \
        python39-pip \
        psmisc \
        tar \
        && \
    microdnf clean all -y && \
    # yum cache files may still exist (and quite large in size)
    rm -rf /var/cache/yum/* && \
    ln -s /usr/bin/python3 /usr/bin/python && \
    ln -s /usr/bin/pip3 /usr/bin/pip


# Install chromedriver version to match Chrome version installed by browserker
RUN cd /opt && wget https://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VERSION/chromedriver_linux64.zip && \
    unzip chromedriver_linux64.zip && \
    rm -f chromedriver_linux64.zip && \
    ln -s /opt/chromedriver /usr/bin/chromedriver

ARG BROWSERKER_UID=1001
RUN usermod --uid $BROWSERKER_UID gitlab

# Install DAST dependencies
COPY requirements.txt /dast-requirements.txt

# Install zapcli/owasp zap seperately from requirements because dependencies are incompatible
COPY --from=compiled_dependencies --chown=root:root /usr/local/bin/mprof /usr/local/bin/mprof
COPY --from=compiled_dependencies --chown=root:root /usr/lib/python3.9/site-packages /usr/lib/python3.9/site-packages
COPY --from=compiled_dependencies --chown=root:root /usr/local/lib/python3.9/site-packages /usr/local/lib/python3.9/site-packages
COPY --from=compiled_dependencies --chown=root:root /usr/local/lib64/python3.9/site-packages /usr/local/lib64/python3.9/site-packages
COPY --from=compiled_dependencies --chown=root:root /root/.cache /root/.cache
RUN pip3 install -r /dast-requirements.txt

# Setup the DAST application
COPY profiling /app/profiling
COPY scripts /app/scripts
COPY resources /app/resources
COPY src /app/src
COPY analyze.py README.md CHANGELOG.md LICENSE /app/
COPY analyze /analyze

# Create the work directories, grant user access
# non-zap users should be able to write to work directories (/output, /zap/wrk)
RUN touch /app/building_for.$BUILDING_FOR && \
    mkdir /data && \
    mkdir /output && \
    chmod 777 /data && \
    chmod 777 /output && \
    mkdir /zap && \
    chmod 777 /zap && \
    find /app/resources -name '*.js' -exec chmod 644 {} \;

## Run as zap, running as root is not supported
USER gitlab
WORKDIR /output

ENV DAST_ZAP_ENABLED=false
ENV DAST_BROWSER_SCAN=true

ENTRYPOINT []
CMD ["/analyze"]
