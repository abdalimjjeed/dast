from .host import Host


class ReadFileSystemServer:

    def __init__(self, run, port=80):
        self.run = run
        self.port = port
        self.host = Host()

    def start(self):

        self.run('docker run --rm '
                 '--name read_file_system_server '
                 f'-p {self.port}:80 '
                 '-v "${PWD}/test/end-to-end/fixtures/check-22-1/nginx.conf":/etc/nginx/conf.d/default.conf '
                 '-v "${PWD}/test/end-to-end/fixtures/check-22-1/index.html:/usr/share/nginx/html/index.html" '
                 '-d openresty/openresty:1.19.9.1-9-buster-fat')

        print(f'Read file system server started at http://{self.host.name()}:{self.port}/')
        return self

    def stop(self):
        self.run('docker rm --force read_file_system_server >/dev/null 2>&1 || true')
        return self
