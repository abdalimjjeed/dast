from .host import Host


class EchoServer:

    def __init__(self, run, port=80, secure_port=443):
        self.run = run
        self.port = port
        self.secure_port = secure_port
        self.host = Host()

    def start(self):
        self.run('docker run --rm '
                 '--name echo-server '
                 f'-p {self.port}:80 '
                 '-v "${PWD}/test/end-to-end/fixtures/echo":/usr/share/nginx/html:ro '
                 '-v "${PWD}/test/end-to-end/fixtures/echo/nginx.conf":/etc/nginx/conf.d/default.conf '
                 '-d openresty/openresty:1.19.9.1-9-buster-fat')

        print(f'Echo server started at http://{self.host.name()}:{self.port}/')
        return self

    def stop(self):
        self.run('docker rm --force echo-server >/dev/null 2>&1 || true')
        return self
