from .host import Host


class RestApiServer:

    def __init__(self, run, port=80):
        self.run = run
        self.port = port
        self.host = Host()

    def start(self):
        self.run('docker run --rm '
                 '--name rest_api_server '
                 f'-p {self.port}:80 '
                 '-v "${PWD}/test/end-to-end/fixtures/rest-api":/usr/share/nginx/html:ro '
                 '-v "${PWD}/test/end-to-end/fixtures/rest-api/nginx.conf":/etc/nginx/conf.d/default.conf '
                 '-d nginx:1.22.0-alpine')

        print(f'REST API server started at http://{self.host.name()}:{self.port}/')
        return self

    def stop(self):
        self.run('docker rm --force rest_api_server >/dev/null 2>&1 || true')
        return self
